# <h1>Alien Invasion</h1>

<h3>A clone demo project from the (1978) "Space Invaders" to interact with Pygame library, and building a 2D game in Python.</h3>

<h4>Specification</h4>
<p>
    In Alien Invasion, the player controls a ship that appears at the bottom center of the screen. The player can move the ship from left to right. When the game begins, a fleet of aliens fills the sky and moves across and down the screen. The player shoots and destroys the aliens. If the player shoots all the aliens, a new fleet appears that moves faster than the previous fleet. If any alien hits the players ship or reaches the bottom of the screen, the player loses the ship. If the player loses three ships, the game ends.
</p>

<h4>Materials Used:</h4>
<ul>
    <li>Environment: Ziron OS (Linux)</li>
    <li>Library: Pygame, STL</li>
    <li>Images used: <a href="https://www.pixabay.com">Pixabay</a>,
                     <a href="https://opengameart.org/">OpenGameArt</a>
    <li>IDE: PyCharm Community Edition</li>
    <li>Linux Command Line</li>
</ul>

<h4>Running the project</h4>
<ol type="1">
    <li>Download and extract the folder, or use the git command `$ git clone https://gitlab.com/Relaxing-Fox/alien-invasion.git`</li>
    <li>Find the folder in the destination folder you extracted it, and using LS (ls) and CD (cd) to that folder</li>
    <li>After finding the folder in the destination folder, use this command in (Linux or Unix) `$ python alien_invasion.py` to play the game</li>
</ol>

<h4>Controllers</h4>
<ul>
    <li>Left and Right Arrows = moving Left and Right</li>
    <li>spacebar = shooting bullets</lil>
    <li>Q = Quit the game</li>
</ul>

<h4>Tasks (v1)</h4>
<ul>
    <li>Creating the window; background color; creating the main method of the game; adding the ship image; bullets, game functions, settings, and ship class; moving the sprite around the screen. <strong>(Done)</strong>
    </li>
    <li>Creating image aliens; creating key event to quit the game; building a fleet of aliens; moving the fleet; shooting aliens; ending the game. <strong>(Done)</strong></li>
    <li>Create the play game button; leveling up; create scoreboard; update the score as aliens are shot down; high scores; display level. <strong>(Done)</strong></li>
</ul>